from setuptools import setup, find_packages

setup(
  name="myhep",
  version='0.0.1',
  description="My personal python package for plotting with CERN's ROOT",
  url="git@gitlab.com:csauer/myhep.git",
  author="Christof Sauer",
  author_email="csauer@cern.ch",
  license="unlicense",
  packages=find_packages(),
  include_package_data = True,
  package_data = {
    # If any package contains *.ini files, include them
    '': ['*.ini'],
  },
  zip_safe=False
)
