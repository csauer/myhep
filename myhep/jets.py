import os
import collections
import configparser

# Get path of this file
this_path = os.path.abspath(os.path.dirname(__file__))


class JetInfo(object):


  def __init__(self, jc_name, var_config=os.path.join(this_path, "etc/jssv.ini"), jet_config=os.path.join(this_path, "etc/jc.ini")):

    # Initialize parser
    self.parser = configparser.ConfigParser()
    # Read all configuration files
    self.parser.read([var_config, jet_config])

    # Current jet collection
    self.setCollectionName(jc_name)


  def setCollectionName(self, name):

    # Check if this name contains a jet collection that is defined in the config files
    candidates = [jc for jc in self.parser.sections() if jc in name or name in jc]
    if candidates:
      self.jc = min(candidates)
    else:
      self.jc = "UnknownCollection"


  def getJssVariableNames(self):

    return filter(lambda x: "fjet_" in x, self.parser.sections())


  def getJccVariables(self, var_name=None):

    var_list = []
    if not var_name:
      for name in self.getJssVariableNames():
        var_list.append(tuple(self.parser.items(name)))

      return var_list
    else:
      return tuple(self.parser.items(var_name))


  def getReconstruction(self, jc=None):

    if not jc:
      return self.parser.get(self.jc, "reconstruction").split(";")
    else:
      return self.parser.get(jc, "reconstruction").split(";")


  def getObject(self, jc=None):

    if not jc:
      return self.parser.get(self.jc, "object").split(";")
    else:
      return self.parser.get(jc, "object").split(";")


  def getGroomer(self, jc=None):

    if not jc:
      return self.parser.get(self.jc, "groomer").split(";")
    else:
      return self.parser.get(jc, "groomer").split(";")


  def getCollectionName(self):

    return self.jc


  def getReconstructionAndObject(self, jc=None, abbreviation=False, rec=True, obj=True, gro=True):

    _rec = self.getReconstruction(jc)
    _obj = self.getObject(jc)
    _gro = self.getGroomer(jc)
    if not abbreviation:
      return "%s %s" % (_rec[1], _obj[1])
    else:
      return "%s%s%s" % (_rec[0] if rec else "", _obj[0] if obj else "", _gro[0] if gro else "")

