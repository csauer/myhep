import os, sys
import math
import ROOT


class Particle(ROOT.TLorentzVector):

  def __init__(self, e, px, py, pz, pid=-999, color=[0,0], mother_id=-1, daughter_id=-1):

    # Call base class constructor
    ROOT.TLorentzVector.__init__(self)
    # Set components of vector and other properties
    self.set(e, px, py, pz, pid, color, mother_id, daughter_id)

  def __getitem__(self, i):

    if i == 0: return self.E()
    if i == 1: return self.Px()
    if i == 2: return self.Px()
    if i == 3: return self.Pz()
    raise Exception("LorentzVector")

  def __str__(self):

    # Get componets
    vec = "(%s, %s, %s, %s)" % (self.E(), self.Px(), self.Py(), self.Pz())
    col = "[%s, %s]" % (self.color[0], self.color[0])
    return "pid, vec, color) : %s, %s, %s" % (self.pid, vec, col)

  def __repr__(self):

    # Get componets
    vec = "(%s, %s, %s, %s)" % (self.E(), self.Px(), self.Py(), self.Pz())
    col = "[%s, %s]" % (self.color[0], self.color[0])
    return "(pid, vec, color) : %s, %s, %s" % (self.pid, vec, col)

  def __add__(self, v):
 
    return Particle(self.E()+v.E(), self.Px()+v.Px(), self.Py()+v.Py(), self.Pz()+v.Pz(), self.pid, self.color)

  def __neg__(self):

    return Particle(-self.E(), -self.Px(), -self.Py(), -self.Pz(), self.pid, self.color)

  def __mul__(self, v):

    if isinstance(v, ROOT.TLorentzVector):
      return self.E()*v.E()-self.Px()*v.Px()-self.Py()*v.Py()-self.Pz()*v.Pz()
    return Particle(self.E()*v, self.Px()*v, self.Py()*v, self.Pz()*v, self.pid, self.color)

  def __rmul__(self, v):

    if isinstance(v, ROOT.TLorentzVector):
      return self.E()*v.E()-self.Px()*v.Px()-self.Py()*v.Py()-self.Pz()*v.Pz()
    return Particle(self.E()*v, self.Px()*v, self.Py()*v, self.Pz()*v, self.pid, self.color)

  def __div__(self, v):

    return Particle(self.E()/v, self.Px()/v, self.Py()/v, self.Pz()/v, self.pid, self.color)

  def cross(self, v):

    return Particle(0.0,
      self.Py()*v.Pz()-self.Pz()*v.Py(),
      self.Pz()*v.Px()-self.Px()*v.Pz(),
      self.Px()*v.Py()-self.Py()*v.Px(),
      self.pid, self.color)

  def boost(self, v):

    rsq = self.M()
    v0 = (self.E()*v.E()-self.Px()*v.Px()-self.Py()*v.Py()-self.Pz()*v.Pz())/rsq;
    c1 = (v.E()+v0)/(rsq+self.E())
    return Particle(v0,
      v.Px()-c1*self.Px(),
      v.Py()-c1*self.Py(),
      v.Pz()-c1*self.Pz(), self.pid, self.color)

  def boostBack(self, v):

    rsq = self.M()
    v0 = (self.E()*v.E()+self.Px()*v.Px()+self.Py()*v.Py()+self.Pz()*v.Pz())/rsq;
    c1 = (v.E()+v0)/(rsq+self.E())
    return Particle(v0,
      v.Px()+c1*self.Px(),
      v.Py()+c1*self.Py(),
      v.Pz()+c1*self.Pz(), self.pid, self.color)

  def set(self, e, px, py, pz, pid, color=(0,0), mother_id=-1, daughter_id=-1):

    # Set components of vector
    self.SetPxPyPzE(px, py, pz, e)
    # Set some other properties
    self.pid = pid
    self.color = color
    self.mother_id = mother_id
    self.daughter_id = daughter_id

  def isColorConnected(self, p):

    # Only valid in large N limit
    return (self.color[0] > 0 and self.color[0] == p.color[1]) or \
      (self.color[1] > 0 and self.color[1] == p.color[0])

