import os, sys
config_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "etc")


# Some global variables
__var_ini__ = os.path.join(config_path, "jssv.ini")

def lookup(label, item, ini):

  """
  Convert a label to a label defined in an ini file

  :param label: label to convert
  :param item: the item in the .ini file that's supposed to be returned
  :param ini: path to an expernal .ini file
  :type label: str
  :type item: str
  :type ini: str
  :rtype str
  """

  if not ini: ini = __var_ini__
  import configparser
  # Read configuration file
  conf = configparser.ConfigParser()
  # Read the config file
  conf.read(ini)
  # Check if `label` is a defined section of the configuration file
  if label not in conf.sections():
    return label
  else:
    if item:
      return conf.get(label, item).encode("ascii")
    else:
      return conf[label]


def name(section, ini=None):

  return lookup(section, "name", ini)


def title(section, ini=None):

  return lookup(section, "title", ini)


def abb(section, ini=None):

  return lookup(section, "abb", ini)


def inunit(section, ini=None):

  return lookup(section, "inunit", ini)


def tounit(section, ini=None):

  return lookup(section, "tounit", ini)


def range(section, ini=None, rtype=str):

   xmin = rtype(lookup(section, "xmin", ini))
   xmax = rtype(lookup(section, "xmax", ini))
   return (xmin, xmax)


def side(section, ini=None):

  return lookup(section, "side", ini)


def tag(section, ini=None):

  return lookup(section, "tag", ini)


def var(section, ini=None):

  return lookup(section, None, ini)


def cut_minmax(section, low=None, up=None, ini=None):

   # Get some strings
  _title = title(section, ini=ini)
  _unit = tounit(section, ini=ini)

  # Handle the cases
  if low and up: return "%s %s < %s < %s %s" % (low, _title, var_name, up, _unit)
  elif low and not up: return "%s > %s %s" % (_title, low, _unit)
  elif not low and up: return "%s < %s %s" % (_title, up, _unit)
  # No cut ...
  else: return ""


def cut_poly(x, y, t_f1):

  tmp_list = []
  for j_par in range(t_f1.GetNpar()):
    par = str(t_f1.GetParameter(j_par))
    if j_par == 0:
      tmp_list.append("%s" % par)
    else:
      tmp_list.append("(%s)*(%s)^%s" % (par, x, j_par))
    return y + opr + "(" + "+".join(tmp_list) + ")"


def set_variable_config(fname):

  global  __var_ini__
  __var_ini__ = fname
  print("[INFO] Updated JSS configuration file: %s" % fname)
