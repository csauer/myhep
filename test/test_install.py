#!/usr/bin/env python

try:
  import myhep
  print("[TEST] Congratulations, myhep is now installed on your system :).")
except ImportError, e:
  print("[WARNING] Module `myhep` is not installed on your system. Something went wrong.")
